# CSI4341 Computer Graphics Final Project
# Terrain
![terrain](https://bitbucket.org/karkisuresh/terrain/raw/aa35c39b566e148ab610be96f0fd164ab87dc182/image.png)

# Video walkthrough
[https://youtu.be/lBVqWpn4wxg](https://youtu.be/lBVqWpn4wxg)

# Running the project

### Using IDE:
- Download or clone the repository
- import the project as maven project to your IDE.
- Run src/core/Main.java

### Using jar file:
- Run terrain.jar (Make sure the res folder is in the same path).

# Controls
- Use **W A S D** to move the camera.
- Use arrow keys to rotate the camera.
- Use mouse scroll wheel to adjust the speed of movement.
- Use **F** to switch between wireframe and normal mode.