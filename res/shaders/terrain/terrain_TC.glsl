#version 430

layout(vertices=16) out;

const int AB=2;
const int BC=3;
const int CD=0;
const int DA=1;

uniform int tFactor;
uniform float tSlope;
uniform float tShift;
uniform vec3 cameraPos;

in vec2 texCoord[];
out vec2 texCoord_TE[];

//f(x) = 600/x^1.8 + 0.3
float tesfunc(float distance){
    return max(0.0, tFactor/pow(distance, tSlope) + tShift);
}

void main() {

    //control tessalation in fist call of shader
    if (gl_InvocationID == 0){

        //mid point of edges of the quads
        vec3 abmid = vec3(gl_in[0].gl_Position + gl_in[3].gl_Position)/2.0;
        vec3 bcmid = vec3(gl_in[3].gl_Position + gl_in[15].gl_Position)/2.0;
        vec3 cdmid = vec3(gl_in[15].gl_Position + gl_in[12].gl_Position)/2.0;
        vec3 damid = vec3(gl_in[12].gl_Position + gl_in[0].gl_Position)/2.0;

        //distance of cameraPos and mid of edges
        float distAB = distance(abmid, cameraPos);
        float distBC = distance(bcmid, cameraPos);
        float distCD = distance(cdmid, cameraPos);
        float distDA = distance(damid, cameraPos);


        gl_TessLevelOuter[AB] = mix(1, gl_MaxTessGenLevel, tesfunc(distAB));
        gl_TessLevelOuter[BC] = mix(1, gl_MaxTessGenLevel, tesfunc(distBC));
        gl_TessLevelOuter[CD] = mix(1, gl_MaxTessGenLevel, tesfunc(distCD));
        gl_TessLevelOuter[DA] = mix(1, gl_MaxTessGenLevel, tesfunc(distDA));

        gl_TessLevelInner[0] = (gl_TessLevelOuter[BC] + gl_TessLevelOuter[DA])/4;
        gl_TessLevelInner[1] = (gl_TessLevelOuter[AB] + gl_TessLevelOuter[CD])/4;
    }

    texCoord_TE[gl_InvocationID] = texCoord[gl_InvocationID];

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

}
