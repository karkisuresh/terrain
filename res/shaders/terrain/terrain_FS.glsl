#version 430

layout(location = 0) out vec4 outputColor;

struct Texture{
    sampler2D diffusemap;
    sampler2D normalmap;
    sampler2D dispmap;
    float heightScale;
    float horizontalScale;
};

in vec3 position_FS;
in vec3 tangent_FS;
in vec2 texCoord_FS;

uniform int tbnRange;
uniform vec3 cameraPos;
uniform sampler2D normalMap;
uniform Texture textures[3];

const vec3 lightDir = vec3(0.1, -1.0, 0.1);
const float intensity = 1.2;

float diffuse (vec3 dir, vec3 normal, float intensity){
    return max(0.04, dot(normal, -dir) * intensity);
}

void main() {

    float dist = length(cameraPos - position_FS);
    float height = position_FS.y;

    vec3 normal = normalize(texture(normalMap, texCoord_FS).rgb);

    vec3 tex0Color = texture(textures[0].diffusemap, texCoord_FS * textures[0].horizontalScale).rgb;
    vec3 tex1Color = texture(textures[1].diffusemap, texCoord_FS * textures[1].horizontalScale).rgb;
    vec3 tex2Color = texture(textures[2].diffusemap, texCoord_FS * textures[2].horizontalScale).rgb;

    float[3] alphas = float[](0,0,0);
    //height based texture
//    if (height > 300){
//        alphas[2] = 1;
//    }
//    else if (height > 200){
//        alphas[1] = 1;
//    }
//    else{
//        alphas[0] = 1;
//    }
    //normal based texture
    if (normal.y > 0.5){
        alphas[2] = 1;
    }   
    else if (normal.y > 0.45){
        alphas[1] = 1;
    }
    else{
        alphas[0] = 1;
    }
    if (dist < tbnRange - 50){
        //apply normalmapping
        float atten = clamp(-dist/(tbnRange - 50) + 1, 0.0, 1.0);
        vec3 bitangent = normalize(cross(tangent_FS, normal));
        mat3 TBN = mat3(bitangent, normal, tangent_FS);

        vec3 bump;
        for (int i=0; i<3; i++){
            bump += (3*(texture(textures[i].normalmap, texCoord_FS * textures[i].horizontalScale).rbg) - 1) * alphas[i];
        }
        bump = normalize(bump);
        bump.xz *= atten;

        normal = normalize(TBN * bump);
    }

    vec3 fragColor = tex0Color * alphas[0] + tex1Color * alphas[1] + tex2Color * alphas[2];

    float diffuse = diffuse(lightDir, normal, intensity);

    fragColor *= diffuse;

    outputColor = vec4(fragColor, 1.0);
}
