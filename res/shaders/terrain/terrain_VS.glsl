#version 430

layout (location = 0) in vec2 position0;   //16 vertices

uniform mat4 objMat;
uniform mat4 worldMat;
uniform float gap;
uniform vec2 location;
uniform int lod;
uniform vec2 index;
uniform int lod_morphing_area[8];
uniform float scaleY;
uniform vec3 cameraPos;
uniform sampler2D heightMap;

out vec2 texCoord;

vec2 morph(vec2 objPos, float height, int morphing_area){

    vec2 morphing = vec2(0, 0);
    vec2 fixLatitude = vec2(0, 0);
    vec2 fixLongitude = vec2(0, 0);
    float distLatitude;
    float distLongitude;

    //bottom left
    if (index == vec2(0,0)){
        fixLatitude = location + vec2(gap, 0);
        fixLongitude = location + vec2(0, gap);
    }

    //bottom right
    else if (index == vec2(1,0)){
        fixLatitude = location;
        fixLongitude = location + vec2(gap, gap);
    }

    else if (index == vec2(0,1)){
        fixLatitude = location + vec2(gap, gap);
        fixLongitude = location;
    }
    else if (index == vec2(1,1)){
        fixLatitude = location + vec2(0, gap);
        fixLongitude = location + vec2(gap, 0);
    }

    distLatitude = length(cameraPos - (worldMat * vec4(fixLatitude.x, height, fixLatitude.y, 1)).xyz);
    distLongitude = length(cameraPos - (worldMat * vec4(fixLongitude.x, height, fixLongitude.y, 1)).xyz);

    if(distLatitude > morphing_area) {
        vec2 fraction = objPos.xy - location;
        if (index == vec2(0,0)){
            float morph = fraction.x - fraction.y;
            if (morph > 0) morphing.x = morph;
        }
        else if (index == vec2(1,0)){
            float morph = gap - fraction.x - fraction.y;
            if (morph > 0) morphing.x = morph;
        }
        else if (index == vec2(0,1)){
            float morph = fraction.x + fraction.y - gap;
            if (morph > 0) morphing.x = -morph;
        }
        else if (index == vec2(1,1)){
            float morph = fraction.y - fraction.x;
            if (morph > 0) morphing.x = -morph;
        }
        else{
            morphing.x = 0;
        }
    }
    if (distLongitude > morphing_area){
        vec2 fraction = objPos.xy - location;
        if (index == vec2(0,0)){
            float morph = fraction.y - fraction.x;
            if (morph > 0) morphing.y = -morph;
        }
        else if (index == vec2(1,0)){
            float morph = fraction.y - (gap - fraction.x);
            if (morph > 0) morphing.y = morph;
        }
        else if (index == vec2(0,1)){
            float morph = gap - fraction.y - fraction.x;
            if (morph > 0) morphing.y = -morph;
        }
        else if (index == vec2(1,1)){
            float morph = fraction.x - fraction.y;
            if (morph > 0) morphing.y = morph;
        }
        else{
            morphing.y = 0;
        }
    }
    return morphing;
}

void main() {
    vec2 objPos = (objMat * vec4(position0.x, 0, position0.y, 1)).xz;

    float height = texture(heightMap, objPos).r;

    //morph only when lod > 0
    if (lod > 0){
        objPos += morph(objPos, height, lod_morphing_area[lod - 1]);
    }
    height = texture(heightMap, objPos).r;
    texCoord = objPos;
    gl_Position = worldMat * vec4(objPos.x, height, objPos.y, 1);
}
