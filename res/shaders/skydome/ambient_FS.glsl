#version 430

in vec3 worldPosition;

layout(location = 0) out vec4 outputColor;

const vec3 baseColor = vec3 (0.529, 0.808, 0.922);

void main(){
    float r = -0.00020 * (worldPosition.y - 2500) + baseColor.x;
    float g = -0.00023 * (worldPosition.y - 2500) + baseColor.y;
    float b = -0.00015 * (worldPosition.y - 2500) + baseColor.z;

    outputColor = vec4(r, g, b, 1.0);
}