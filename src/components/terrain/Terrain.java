package components.terrain;

import core.kernel.Camera;
import core.scene.Node;

public class Terrain extends Node {

    private TerrainConfig terrainConfig;

    public void init(String config){

        terrainConfig = new TerrainConfig();
        terrainConfig.loadFile(config);
        addChild(new QuadTree(terrainConfig));

    }

    public void updateQuadTree(){

        if(Camera.getInstance().isCameraMoved()){
            ((QuadTree) getChildren().get(0)).updateQuadTree();
        }

    }

    public TerrainConfig getConfig() {
        return terrainConfig;
    }

    public void setConfig(TerrainConfig config) {
        this.terrainConfig = config;
    }

}
