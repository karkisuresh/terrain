#version 430
//we have triangles; Tesselation evaluation shader converts quads into triangles

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

struct Texture{
    sampler2D diffusemap;
    sampler2D normalmap;
    sampler2D dispmap;
    float heightScale;
    float horizontalScale;
};

uniform mat4 m_ViewProjection;
uniform sampler2D normalMap;
uniform vec3 cameraPos;
uniform Texture textures[3];
uniform int tbnRange;

in vec2 texCoord_GS[];
out vec2 texCoord_FS;
out vec3 position_FS;
out vec3 tangent_FS; //tbn normal mapping

vec3 tangent;
vec3 displacement[3];

void getTangent(){
    //https://learnopengl.com/Advanced-Lighting/Normal-Mapping
    vec3 pos1 = gl_in[0].gl_Position.xyz;
    vec3 pos2 = gl_in[1].gl_Position.xyz;
    vec3 pos3 = gl_in[2].gl_Position.xyz;

    vec3 edge1 = pos2 - pos1;
    vec3 edge2 = pos3 - pos1;

    vec2 uv1 = texCoord_GS[0];
    vec2 uv2 = texCoord_GS[1];
    vec2 uv3 = texCoord_GS[2];

    vec2 deltauv1 = uv2 - uv1;
    vec2 deltauv2 = uv3 - uv1;

    float f = 1.0/(deltauv1.x * deltauv2.y - deltauv1.y * deltauv2.x);
    tangent = normalize((edge1 * deltauv2.y - edge2 * deltauv1.y) * f);

}

void main() {

    for (int i=0; i<3; i++){
        displacement[i] = vec3(0,0,0);
    }

    float dist = (distance(gl_in[0].gl_Position.xyz, cameraPos) + distance(gl_in[1].gl_Position.xyz, cameraPos)
    + distance(gl_in[2].gl_Position.xyz, cameraPos))/3;  //distance of the triangle

    if (dist < tbnRange){
        getTangent();
        for (int k=0; k<gl_in.length(); k++){
            displacement[k] = vec3(0, 1, 0);
            float height = gl_in[k].gl_Position.y;
            vec3 normal = normalize(texture(normalMap, texCoord_GS[k]).rgb);
            float[3] alphas = float[](0,0,0);
            //height based texture
//            if (height > 300){
//                alphas[2] = 1;
//            }
//            else if (height > 200){
//                alphas[1] = 1;
//            }
//            else{
//                alphas[0] = 1;
//            }
            //normal based texture
            if (normal.y > 0.5){
                alphas[2] = 1;
            }
            else if (normal.y > 0.45){
                alphas[1] = 1;
            }
            else{
                alphas[0] = 1;
            }
            float scale = 0;
            for (int i=0; i<3; i++){
                scale += texture(textures[i].dispmap, texCoord_GS[k] * textures[i].horizontalScale).r
                * textures[i].heightScale * alphas[i];
            }

            float attenuation = clamp(-distance(gl_in[k].gl_Position.xyz, cameraPos)/(tbnRange-50) + 1, 0.0, 1.0);
            scale *= attenuation;

            displacement[k] *= scale;
        }
    }

    for (int i=0; i<gl_in.length(); ++i){
        vec4 position = gl_in[i].gl_Position + vec4(displacement[i], 0);
        gl_Position = m_ViewProjection * position;
        texCoord_FS = texCoord_GS[i];
        position_FS = (position).xyz;
        tangent_FS = tangent;
        EmitVertex();
    }

//    vec4 position = gl_in[0].gl_Position;
//    gl_Position = m_ViewProjection * position;
//    EmitVertex();

    EndPrimitive();
}
