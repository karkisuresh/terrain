package core.scene;

import core.math.Transform;

public abstract class Component {

    private GameObject parent;  //acess game object from the component

    public void update(){}
    public void input(){}
    public void render(){}

    public GameObject getParent() {
        return parent;
    }

    public void setParent(GameObject parent) {
        this.parent = parent;
    }

    public Transform getWorldTransform(){
        return getParent().getWorldTransform();
    }

    public Transform getObjectTransform(){
        return getParent().getObjectTransform();
    }

}
