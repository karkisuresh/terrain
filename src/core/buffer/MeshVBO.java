package core.buffer;

import core.model.Mesh;
import core.utils.BufferUtil;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public class MeshVBO implements VertexBufferObject {

    private int vertexBufferId;
    private int indicesBufferId;
    private int vertexArrayObjectId;
    private int size;  //size of the faces of the mesh

    public MeshVBO(){
        vertexBufferId = GL15.glGenBuffers();
        indicesBufferId = GL15.glGenBuffers();
        vertexArrayObjectId = GL30.glGenVertexArrays();
    }

    public void allocate(Mesh mesh){
        size = mesh.getIndices().length;
        GL30.glBindVertexArray(vertexArrayObjectId);  //bind vertex array buffer
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexBufferId);
        //fill vertex buffer
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, BufferUtil.createFlippedBufferAOS(mesh.getVertices()),
                GL15.GL_STATIC_DRAW);
        //allocate indices buffer
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBufferId);
        //fill
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, BufferUtil.createFlippedBuffer(mesh.getIndices()),
                GL15.GL_STATIC_DRAW);
        //pointers to the buffers and specific data of buffer
        //3 vertexAttributePointer (position, normal, textureCoord)
        //stride is 8 because we have 3 for position, 3 for normal and 2 for texCoord
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, Float.BYTES * 8, 0);
        GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, Float.BYTES * 8,
                Float.BYTES * 3);
        GL20.glVertexAttribPointer(2, 3, GL11.GL_FLOAT, false, Float.BYTES * 8,
                Float.BYTES * 6);

        GL30.glBindVertexArray(0);   //zero to break the existing vertex array object binding.
    }

    @Override
    public void draw(){

        GL30.glBindVertexArray(vertexArrayObjectId);

        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);

        GL11.glDrawElements(GL11.GL_TRIANGLES, size, GL11.GL_UNSIGNED_INT, 0);

        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);

        GL30.glBindVertexArray(0);  //zero to break the existing vertex array object binding.
    }

    @Override
    public void delete(){

        GL30.glBindVertexArray(vertexArrayObjectId);

        GL15.glDeleteBuffers(vertexBufferId);
        GL15.glDeleteBuffers(indicesBufferId);
        GL30.glDeleteVertexArrays(vertexArrayObjectId);

        GL30.glBindVertexArray(0);  //zero to break the existing vertex array object binding.

    }

}
