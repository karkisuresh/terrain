#version 430
//we have triangles; Tesselation evaluation shader converts quads into triangles

layout(triangles) in;
layout(line_strip, max_vertices = 4) out;

struct Texture{
    sampler2D diffusemap;
    sampler2D normalmap;
    sampler2D dispmap;
    float heightScale;
    float horizontalScale;
};

uniform mat4 m_ViewProjection;
uniform sampler2D normalMap;
uniform vec3 cameraPos;
uniform Texture textures[2];
uniform int tbnRange;

in vec2 texCoord_GS[];

vec3 tangent;
vec3 displacement[3];

void getTangent(){
    vec3 v0 = gl_in[0].gl_Position.xyz;
    vec3 v1 = gl_in[1].gl_Position.xyz;
    vec3 v2 = gl_in[2].gl_Position.xyz;

    vec3 e1 = v1 - v0;
    vec3 e2 = v2 - v0;

    vec2 uv0 = texCoord_GS[0];
    vec2 uv1 = texCoord_GS[1];
    vec2 uv2 = texCoord_GS[2];

    vec2 deltauv1 = uv1 - uv0;
    vec2 deltauv2 = uv2 - uv0;

    float r = 1.0/(deltauv1.x * deltauv2.y - deltauv1.y * deltauv2.x);
    tangent = normalize((e1 * deltauv2.y - e2 * deltauv1.y) * r);

}

void main() {

    for (int i=0; i<3; i++){
        displacement[i] = vec3(0,0,0);
    }

    float dist = (distance(gl_in[0].gl_Position.xyz, cameraPos) + distance(gl_in[1].gl_Position.xyz, cameraPos)
    + distance(gl_in[2].gl_Position.xyz, cameraPos))/3;  //distance of the triangle

    if (dist < tbnRange){
        getTangent();
        for (int k=0; k<gl_in.length(); k++){
            displacement[k] = vec3(0, 1, 0);
            float height = gl_in[k].gl_Position.y;
            vec3 normal = normalize(texture(normalMap, texCoord_GS[k]).rgb);
            float[2] alphas = float[](0,0);
            if (normal.y > 0.5){
                alphas[1] = 1;
            }
            else{
                alphas[0] = 1;
            }
            float scale = 0;
            for (int i=0; i<2; i++){
                scale += texture(textures[i].dispmap, texCoord_GS[k] * textures[i].horizontalScale).r
                * textures[i].heightScale * alphas[i];
            }

            float attenuation = clamp(-distance(gl_in[k].gl_Position.xyz, cameraPos)/(tbnRange-50) + 1, 0.0, 1.0);
            scale *= attenuation;

            displacement[k] *= scale;
        }
    }

    for (int i=0; i<gl_in.length(); ++i){
        vec4 position = gl_in[i].gl_Position + vec4(displacement[i], 0);
        gl_Position = m_ViewProjection * position;
        EmitVertex();
    }

    vec4 position = gl_in[0].gl_Position + vec4(displacement[0], 0);
    gl_Position = m_ViewProjection * position;
    EmitVertex();

    EndPrimitive();
}
