package components.bump;

import core.texturing.Texture2D;
import org.lwjgl.opengl.*;

public class NormalMapRenderer {

    private float strength;

    private Texture2D normalMap;
    private NormalMapShader shader;
    private int N; //generates normals for just quad heightmaps

    public NormalMapRenderer(int N){
        this.N = N;
        shader = NormalMapShader.getInstance();
        normalMap = new Texture2D();
        normalMap.generate();
        normalMap.bind();
        normalMap.bilinearFliter();
        //generate storage for texture
        GL42.glTexStorage2D(GL11.GL_TEXTURE_2D, (int)(Math.log(N)/Math.log(2)), GL30.GL_RGBA32F, N, N);
    }

    public void render(Texture2D heightMap){
        shader.bind();
        shader.updateUniforms(heightMap, N, strength);
        GL42.glBindImageTexture(0, normalMap.getId(), 0, false, 0, GL15.GL_WRITE_ONLY,
                GL30.GL_RGBA32F);
        GL43.glDispatchCompute(N/16, N/16, 1);
        GL11.glFinish();
        normalMap.bind();
        normalMap.bilinearFliter();
    }

    public float getStrength() {
        return strength;
    }

    public void setStrength(float strength) {
        this.strength = strength;
    }

    public int getN() {
        return N;
    }

    public void setN(int n) {
        N = n;
    }

    public Texture2D getNormalMap() {
        return normalMap;
    }

    public void setNormalMap(Texture2D normalMap) {
        this.normalMap = normalMap;
    }


}
