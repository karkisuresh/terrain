package components.skydome;

import core.scene.GameObject;
import core.shaders.Shader;
import core.utils.ResourceLoader;

public class AmbientShader extends Shader {

    private static AmbientShader instance = null;

    public static AmbientShader getInstance(){
        if (instance == null){
            instance = new AmbientShader();
        }
        return instance;
    }

    protected AmbientShader(){
        super();
        addVertexShader(ResourceLoader.loadShader("shaders/skydome/ambient_VS.glsl"));
        addFragmentShader(ResourceLoader.loadShader("shaders/skydome/ambient_FS.glsl"));
        compileShader();

        addUniform("m_ModelViewProjection");
        addUniform("m_World");
    }

    public void updateUniforms(GameObject object){
        setUniform("m_ModelViewProjection", object.getWorldTransform().getMVP());
        setUniform("m_World", object.getWorldTransform().getWorldMatrix());
    }

}
