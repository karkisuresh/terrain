package components.terrain;

import components.bump.NormalMapRenderer;
import core.model.Material;
import core.texturing.Texture2D;
import core.utils.BufferUtil;
import core.utils.Util;
import org.lwjgl.opengl.GL11;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

public class TerrainConfig {

    private float scaleY;
    private float scaleXZ;   //horizontal scale

    private int[] lodRange = new int[8];
    private int[] lodMorphing = new int[8];

    private int tessFactor;
    private float tessSlope;
    private float tessShift;
    private int tbnRange;
    private List<Material> materials = new ArrayList<>();

    private Texture2D heightMap;

    private Texture2D normalMap;

    private FloatBuffer terrainHeight;

    public void loadFile(String file){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null){
                String[] tokens = line.split(" ");
                tokens = Util.removeEmptyStrings(tokens);

                if(tokens.length == 0) continue;

                if (tokens[0].equals("scaleY")){
                    setScaleY(Float.valueOf(tokens[1]));
                }
                if (tokens[0].equals("scaleXZ")){
                    setScaleXZ(Float.valueOf(tokens[1]));
                }
                if (tokens[0].equals("tessFactor")){
                    setTessFactor(Integer.valueOf(tokens[1]));
                }
                if (tokens[0].equals("tessSlope")){
                    setTessSlope(Float.valueOf(tokens[1]));
                }
                if (tokens[0].equals("tessShift")){
                    setTessShift(Float.valueOf(tokens[1]));
                }
                if (tokens[0].equals("heightMap")){
                    setHeightMap(new Texture2D(tokens[1]));
                    getHeightMap().bind();
                    getHeightMap().bilinearFliter();

                    calcTerrainHeight();

                    NormalMapRenderer normalMapRenderer = new NormalMapRenderer(getHeightMap().getWidth());
                    normalMapRenderer.setStrength(10);
                    normalMapRenderer.render(getHeightMap());
                    setNormalMap(normalMapRenderer.getNormalMap());
                }
                if(tokens[0].equals("#lod_ranges")){
                    for(int i=0; i<8; i++){
                        line = reader.readLine();
                        tokens = line.split(" ");
                        tokens = Util.removeEmptyStrings(tokens);
                        if (tokens[0].equals("lod" + (i+1) + "_range")){
                            if (Integer.valueOf(tokens[1]) == 0){
                                lodRange[i] = 0;
                                lodMorphing[i] = 0;
                            }
                            else{
                                setLodRange(i, Integer.valueOf(tokens[1]));
                            }
                        }
                    }
                }
                if(tokens[0].equals("tbn_range")){
                    setTbnRange(Integer.valueOf(tokens[1]));
                }
                if(tokens[0].equals("texture")){
                    getMaterials().add(new Material());
                }
                if(tokens[0].equals("diff_texture")){
                    Texture2D diffusemap = new Texture2D(tokens[1]);
                    diffusemap.bind();
                    diffusemap.trilinear();
                    getMaterials().get(materials.size() - 1).setDiffusemap(diffusemap);
                }
                if(tokens[0].equals("nrm_texture")){
                    Texture2D normalmap = new Texture2D(tokens[1]);
                    normalmap.bind();
                    normalmap.trilinear();
                    getMaterials().get(materials.size() - 1).setNormalmap(normalmap);
                }
                if(tokens[0].equals("disp_texture")){
                    Texture2D dispmap = new Texture2D(tokens[1]);
                    dispmap.bind();
                    dispmap.trilinear();
                    getMaterials().get(materials.size() - 1).setDisplacemap(dispmap);
                }
                if(tokens[0].equals("heightScaling")){
                    getMaterials().get(materials.size() - 1).setDisplaceScale(Float.valueOf(tokens[1]));
                }
                if(tokens[0].equals("horizontalScaling")){
                    getMaterials().get(materials.size() - 1).setHorizontalScale(Float.valueOf(tokens[1]));
                }

            }
            reader.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * returns morphingArea for specific lod. MorphingArea is the space where morphing function morphs lod to lower
     * level of detail. morphing area indicates when to start the morphing
     * @param lod
     */
    private int updateMorhphingArea(int lod){
        return (int) ((scaleXZ/ QuadTree.getRootNodes()) / (Math.pow(2, lod)));
    }

    private void setLodRange(int index, int lod_range){
        this.lodRange[index] = lod_range;
        lodMorphing[index] = lod_range - updateMorhphingArea(index+1);
    }

    public float getScaleY() {
        return scaleY;
    }

    public void setScaleY(float scaleY) {
        this.scaleY = scaleY;
    }

    public float getScaleXZ() {
        return scaleXZ;
    }

    public void setScaleXZ(float scaleXZ) {
        this.scaleXZ = scaleXZ;
    }

    public int[] getLodRange() {
        return lodRange;
    }

    public void setLodRange(int[] lodRange) {
        this.lodRange = lodRange;
    }

    public int[] getLodMorphing() {
        return lodMorphing;
    }

    public void setLodMorphing(int[] lodMorphing) {
        this.lodMorphing = lodMorphing;
    }

    public int getTessFactor() {
        return tessFactor;
    }

    public void setTessFactor(int tessFactor) {
        this.tessFactor = tessFactor;
    }

    public float getTessSlope() {
        return tessSlope;
    }

    public void setTessSlope(float tessSlope) {
        this.tessSlope = tessSlope;
    }

    public float getTessShift() {
        return tessShift;
    }

    public void setTessShift(float tessShift) {
        this.tessShift = tessShift;
    }

    public Texture2D getHeightMap() {
        return heightMap;
    }

    public void setHeightMap(Texture2D heightMap) {
        this.heightMap = heightMap;
    }

    public Texture2D getNormalMap() {
        return normalMap;
    }

    public void setNormalMap(Texture2D normalMap) {
        this.normalMap = normalMap;
    }

    public int getTbnRange() {
        return tbnRange;
    }

    public void setTbnRange(int tbnRange) {
        this.tbnRange = tbnRange;
    }

    public List<Material> getMaterials() {
        return materials;
    }

    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }

    public FloatBuffer getTerrainHeight() {
        return terrainHeight;
    }

    public void setTerrainHeight(FloatBuffer terrainHeight) {
        this.terrainHeight = terrainHeight;
    }

    public void calcTerrainHeight(){
        terrainHeight = BufferUtil.createFloatBuffer(getHeightMap().getWidth() * getHeightMap().getHeight());
        heightMap.bind();
        GL11.glGetTexImage(GL11.GL_TEXTURE_2D, 0, GL11.GL_RED, GL11.GL_FLOAT, terrainHeight);
    }

}
