package components.terrain;

import core.buffer.PatchVBO;
import core.configs.Default;
import core.configs.Wireframe;
import core.kernel.Camera;
import core.kernel.RenderContext;
import core.math.Vec2f;
import core.math.Vec3f;
import core.renderer.RenderInfo;
import core.renderer.Renderer;
import core.scene.GameObject;
import core.scene.Node;
import core.utils.Constants;

public class TerrainNode extends GameObject {

    private boolean isLeaf;
    private TerrainConfig config;
    private int lod;
    private Vec2f location;  //location on the quad tree;
    private Vec3f worldPos;
    private Vec2f index;  //specifies where the location is within one patch consisting of 4 children
    private float gap;  //side length of one patch
    private PatchVBO buffer;

    public TerrainNode(PatchVBO buffer, TerrainConfig config, Vec2f location, int lod, Vec2f index){
        this.buffer = buffer;
        this.isLeaf = true;
        this.config = config;
        this.location = location;
        this.lod = lod;
        this.index = index;
        this.gap = 1f/(QuadTree.getRootNodes() * (float)(Math.pow(2, lod)));
        //local transformation transform the position within the quadtree
        Vec3f localScaling = new Vec3f(gap, 0, gap);
        Vec3f localTranslation = new Vec3f(location.getX(), 0, location.getY());

        getObjectTransform().setScaling(localScaling);
        getObjectTransform().setTranslation(localTranslation);

        //positions quad in world space
        getWorldTransform().setScaling(new Vec3f(config.getScaleXZ(), config.getScaleY(), config.getScaleXZ()));
        getWorldTransform().setTranslation(new Vec3f(-config.getScaleXZ()/2f,0, -config.getScaleXZ()/2f));

        Renderer renderer = new Renderer();
        renderer.setVbo(buffer);
        renderer.setRenderInfo(new RenderInfo(new Default(), TerrainShader.getInstance()));
        addComponent(Constants.RENDERER_COMPONENT, renderer);

        Renderer wfRenderer = new Renderer();
        wfRenderer.setVbo(buffer);
        wfRenderer.setRenderInfo(new RenderInfo(new Default(), WireframeShader.getInstance()));
        addComponent(Constants.WF_RENDERER_COMPONENT, wfRenderer);

        computeWorldPos();
        updateQuadTree();

    }

    public void render(){
        //only render leaves of the quadtree
        if (isLeaf){
            if (RenderContext.getInstance().isWireframe()){
                getComponents().get(Constants.WF_RENDERER_COMPONENT).render();
            }
            else {
                getComponents().get(Constants.RENDERER_COMPONENT).render();
            }
        }

        for (Node child: getChildren()){
            child.render();
        }
    }

    public void updateQuadTree(){
        updateChildNodes();
        for (Node child: getChildren()){
            ((TerrainNode) child).updateQuadTree();
        }
    }

    private void addChildNodes(int lod){
        if (isLeaf){
            isLeaf = false;
        }
        if (getChildren().size() == 0){
            for (int i=0; i<2; i++){
                for (int j=0; j<2; j++){
                    addChild(new TerrainNode(buffer, config, location.add(new Vec2f(i*gap/2f, j*gap/2f)), lod,
                            new Vec2f(i, j)));
                }
            }
        }
    }

    private void removeChildNodes(){

        if (!isLeaf){
            isLeaf = true;
        }
        if (getChildren().size() != 0){
            getChildren().clear();
        }
    }

    private void updateChildNodes(){
        float distance = (Camera.getInstance().getPosition().sub(worldPos)).length();

        if (distance < config.getLodRange()[lod]){
            addChildNodes(lod+1);
        }
        else if (distance >= config.getLodRange()[lod]){
            removeChildNodes();
        }
    }

    public void computeWorldPos(){
        Vec2f loc = location.add(gap/2f).mul(config.getScaleXZ()).sub(config.getScaleXZ()/2f); //horizontal loc in world

        float z = getTerrainHeight(loc.getX(), loc.getY()); //vertical height

        this.worldPos = new Vec3f(loc.getX(), z, loc.getY());

    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        isLeaf = leaf;
    }

    public TerrainConfig getConfig() {
        return config;
    }

    public void setConfig(TerrainConfig config) {
        this.config = config;
    }

    public int getLod() {
        return lod;
    }

    public void setLod(int lod) {
        this.lod = lod;
    }

    public Vec2f getLocation() {
        return location;
    }

    public void setLocation(Vec2f location) {
        this.location = location;
    }

    public Vec3f getWorldPos() {
        return worldPos;
    }

    public void setWorldPos(Vec3f worldPos) {
        this.worldPos = worldPos;
    }

    public Vec2f getIndex() {
        return index;
    }

    public void setIndex(Vec2f index) {
        this.index = index;
    }

    public float getGap() {
        return gap;
    }

    public void setGap(float gap) {
        this.gap = gap;
    }

    public PatchVBO getBuffer() {
        return buffer;
    }

    public void setBuffer(PatchVBO buffer) {
        this.buffer = buffer;
    }

    public float getTerrainHeight(float x, float z){

        float height = 0;
        Vec2f position = new Vec2f();
        position.setX(x);
        position.setY(z);
        position = position.add(config.getScaleXZ()/2f);
        position = position.div(config.getScaleXZ());
        Vec2f floorPosition = new Vec2f((int) Math.floor(position.getX()), (int)Math.floor(position.getY()));
        position = position.sub(floorPosition);
        position = position.mul(config.getHeightMap().getWidth());
        //corners of terrain around coordinate of xz
        int x0 = (int) Math.floor(position.getX());
        int x1 = x0 + 1;
        int z0 = (int) Math.floor(position.getY());
        int z1 = z0 + 1;
        float h0 = config.getTerrainHeight().get(config.getHeightMap().getWidth() * z0 + x0);
        float h1 = config.getTerrainHeight().get(config.getHeightMap().getWidth() * z0 + x1);
        float h2 = config.getTerrainHeight().get(config.getHeightMap().getWidth() * z1 + x0);
        float h3 = config.getTerrainHeight().get(config.getHeightMap().getWidth() * z1 + x1);

        float u = position.getX() - x0;
        float v = position.getY() - z0;

        float du, dv;

        if(u > v){
            du = h1 - h0;
            dv = h3 - h1;
        }
        else{
            du = h3 - h2;
            dv = h2 - h0;
        }
        height = h0 + (du * u) + (dv * v);
        height *= config.getScaleY();
        return height;
    }


}
