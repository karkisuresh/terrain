package core.kernel;

import components.skydome.Skydome;
import components.terrain.Terrain;
import core.configs.Default;
import org.lwjgl.glfw.GLFW;

/**
 * 
 * @author oreon3D
 * The RenderingEngine manages the render calls of all 3D entities
 * with shadow rendering and post processing effects
 *
 */
public class RenderingEngine {
	
	private Window window;

	private Skydome skydome;
	private Terrain terrain;
	
	public RenderingEngine()
	{
	    window = Window.getInstance();
	    //added by Suresh Karki
	    skydome = new Skydome();
	    terrain = new Terrain();
	}
	
	public void init()
	{
	    window.init();
	    terrain.init("./res/settings/settings.txt");
	}

	public void render()
	{	
		Camera.getInstance().update();

        Default.clearScreen();
        skydome.render();

        terrain.updateQuadTree();
        terrain.render();

		
		// draw into OpenGL window
		window.render();
	}
	
	public void update(){
		if (Input.getInstance().isKeyPushed(GLFW.GLFW_KEY_F)){
		    if (RenderContext.getInstance().isWireframe()) RenderContext.getInstance().setWireframe(false);
		    else RenderContext.getInstance().setWireframe(true);
        }
	}
	
	public void shutdown(){}
}
