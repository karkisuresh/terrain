package core.renderer;

import core.buffer.VertexBufferObject;
import core.scene.Component;

public class Renderer extends Component {

    private VertexBufferObject vbo;
    private RenderInfo renderInfo;

    public Renderer(){}

    public VertexBufferObject getVbo() {
        return vbo;
    }

    public void setVbo(VertexBufferObject vbo) {
        this.vbo = vbo;
    }

    public RenderInfo getRenderInfo() {
        return renderInfo;
    }

    public void setRenderInfo(RenderInfo renderInfo) {
        this.renderInfo = renderInfo;
    }

    /**
     * Draws/renders in GPU memory
     */
    public void render(){
        renderInfo.getConfig().enable();
        renderInfo.getShader().bind();
        renderInfo.getShader().updateUniforms(getParent());

        getVbo().draw();
        renderInfo.getConfig().disable();
    }

}
