package core.scene;

import core.math.Transform;

import java.util.ArrayList;
import java.util.List;

public class Node {

    private Node parent;
    private List<Node> children;
    private Transform worldTransform;
    private Transform objectTransform;

    public Node(){
        setWorldTransform(new Transform());
        setObjectTransform(new Transform());
        setChildren(new ArrayList<>());
    }

    public void addChild(Node child){
        child.setParent(this);
        children.add(child);
    }

    public void render(){
        for (Node child: children){
            child.render();
        }
    }

    public void shutdown(){
        for (Node child: children){
            child.shutdown();
        }
    }

    public void input(){
        for (Node child: children){
            child.input();
        }
    }

    public void update(){
        for (Node child: children){
            child.update();
        }
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public List<Node> getChildren() {
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public Transform getWorldTransform() {
        return worldTransform;
    }

    public void setWorldTransform(Transform worldTransform) {
        this.worldTransform = worldTransform;
    }

    public Transform getObjectTransform() {
        return objectTransform;
    }

    public void setObjectTransform(Transform objectTransform) {
        this.objectTransform = objectTransform;
    }

}
