package core.buffer;

import core.math.Vec2f;
import core.utils.BufferUtil;
import org.lwjgl.opengl.*;

public class PatchVBO implements VertexBufferObject {

    private int vertexBufferId;
    private int vertexArrayObjectId;
    private int size;  //size of the faces of the mesh

    public PatchVBO(){
        vertexBufferId = GL15.glGenBuffers();
        vertexArrayObjectId = GL30.glGenVertexArrays();
    }

    public void allocate(Vec2f[] vertices, int patchsize){

        size = vertices.length;
        GL30.glBindVertexArray(vertexArrayObjectId);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertexBufferId);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, BufferUtil.createFlippedBuffer(vertices), GL15.GL_STATIC_DRAW);
        //GL_STATIC_DRAW
        //The data store contents will be modified once and used many times as the source for GL drawing commands.

        GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, Float.BYTES * 2, 0);
        GL40.glPatchParameteri(GL40.GL_PATCH_VERTICES, patchsize);

        GL30.glBindVertexArray(0); //unbind vertex array
    }

    @Override
    public void draw(){

        GL30.glBindVertexArray(vertexArrayObjectId);
        GL20.glEnableVertexAttribArray(0);
        GL11.glDrawArrays(GL40.GL_PATCHES, 0, size);

        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);

    }

    @Override
    public void delete(){

        GL30.glBindVertexArray(vertexArrayObjectId);
        GL15.glDeleteBuffers(vertexBufferId);
        GL30.glDeleteVertexArrays(vertexArrayObjectId);
        GL30.glBindVertexArray(0);

    }


}
