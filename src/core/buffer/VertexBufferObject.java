package core.buffer;


public interface VertexBufferObject {
    void draw();
    void delete();
}
