package components.skydome;

import core.buffer.MeshVBO;
import core.configs.CCW;
import core.kernel.RenderContext;
import core.math.Vec3f;
import core.model.Mesh;
import core.model.Model;
import core.renderer.RenderInfo;
import core.renderer.Renderer;
import core.scene.GameObject;
import core.utils.Constants;
import core.utils.objloader.OBJLoader;

public class Skydome extends GameObject {

    public Skydome(){

        Model model = new OBJLoader().load("./res/models/dome", "dome.obj", null)[0];
        Mesh mesh = model.getMesh();
        MeshVBO meshVBO = new MeshVBO();
        //mesh data to GPU memory using VBO
        meshVBO.allocate(mesh);

        Renderer renderer = new Renderer();
        renderer.setVbo(meshVBO);
        renderer.setRenderInfo(new RenderInfo(new CCW(), AmbientShader.getInstance()));
        addComponent(Constants.RENDERER_COMPONENT, renderer);
        getWorldTransform().setScaling(new Vec3f(Constants.ZFAR * 0.5f,
                Constants.ZFAR * 0.5f,
                Constants.ZFAR * 0.5f));
    }

    @Override
    public void render(){
        if (!RenderContext.getInstance().isWireframe()){
            super.render();
        }
    }
}
