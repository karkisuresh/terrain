package components.terrain;

import core.buffer.PatchVBO;
import core.math.Vec2f;
import core.scene.Node;

public class QuadTree extends Node {

    private static int rootNodes = 8; //root patches so we have 64 rootNodes, 8 in each direction

    public QuadTree(TerrainConfig config){

        PatchVBO buffer = new PatchVBO();
        buffer.allocate(generatePatch(), 16);
        for (int i=0; i<rootNodes; i++){
            for (int j=0; j<rootNodes; j++){
                addChild(new TerrainNode(buffer, config, new Vec2f(i/(float) rootNodes, j/(float) rootNodes),
                        0, new Vec2f(i, j)));
            }
        }

        //getWorldTransform().setScaling(new Vec3f(config.getScaleXZ(), config.getScaleY(), config.getScaleXZ()));
        //getWorldTransform().setTranslation(new Vec3f(config.getScaleXZ()/2f, 0, config.getScaleXZ()/2f));
    }

    public void updateQuadTree(){
        for (Node child: getChildren()){
            //update quadtree for every children
            ((TerrainNode) child).updateQuadTree();
        }
    }

    public Vec2f[] generatePatch(){

        Vec2f[] vertices = new Vec2f[16];
        int index= 0;
        // first 4 vertices define a line
        vertices[index++] = new Vec2f(0,0);
        vertices[index++] = new Vec2f(0.333f, 0);
        vertices[index++] = new Vec2f(0.666f, 0);
        vertices[index++] = new Vec2f(1, 0);

        //do four times to get 16 vertices
        vertices[index++] = new Vec2f(0,0.333f);
        vertices[index++] = new Vec2f(0.333f, 0.333f);
        vertices[index++] = new Vec2f(0.666f, 0.333f);
        vertices[index++] = new Vec2f(1, 0.333f);

        vertices[index++] = new Vec2f(0,0.666f);
        vertices[index++] = new Vec2f(0.333f, 0.666f);
        vertices[index++] = new Vec2f(0.666f, 0.666f);
        vertices[index++] = new Vec2f(1, 0.666f);

        vertices[index++] = new Vec2f(0,1);
        vertices[index++] = new Vec2f(0.333f, 1);
        vertices[index++] = new Vec2f(0.666f, 1);
        vertices[index++] = new Vec2f(1, 1);

        return vertices;
    }

    public static int getRootNodes() {
        return rootNodes;
    }

    public static void setRootNodes(int rootNodes) {
        QuadTree.rootNodes = rootNodes;
    }

}
