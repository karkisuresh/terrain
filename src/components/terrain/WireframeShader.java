package components.terrain;

import core.kernel.Camera;
import core.scene.GameObject;
import core.shaders.Shader;
import core.utils.ResourceLoader;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;

public class WireframeShader extends Shader {

    private static WireframeShader instance = null;

    public static WireframeShader getInstance(){
        if (instance == null){
            instance =  new WireframeShader();
        }
        return instance;
    }

    protected WireframeShader(){
        super();
        addVertexShader(ResourceLoader.loadShader("/shaders/terrain/terrain_VS.glsl"));
        addTessellationControlShader(ResourceLoader.loadShader("shaders/terrain/terrain_TC.glsl"));
        addTessellationEvaluationShader(ResourceLoader.loadShader("shaders/terrain/terrain_TE.glsl"));
        addGeometryShader(ResourceLoader.loadShader("shaders/terrain/wire_GS.glsl"));
        addFragmentShader(ResourceLoader.loadShader("shaders/terrain/wire_FS.glsl"));

        compileShader();

        addUniform("objMat");
        addUniform("worldMat");
        addUniform("m_ViewProjection");
        addUniform("gap");
        addUniform("location");
        addUniform("lod");
        addUniform("index");
        addUniform("scaleY");
        addUniform("cameraPos");
        addUniform("tFactor");
        addUniform("tSlope");
        addUniform("tShift");
        addUniform("heightMap");
        addUniform("tbnRange");
        for (int i=0; i<2; i++){
            addUniform("textures[" + i + "].dispmap");
            addUniform("textures[" + i + "].heightScale");
            addUniform("textures[" + i + "].horizontalScale");
        }
    }

    public void updateUniforms(GameObject object){
        setUniform("m_ViewProjection", Camera.getInstance().getViewProjectionMatrix());
        setUniform("objMat", object.getObjectTransform().getWorldMatrix());
        setUniform("worldMat", object.getWorldTransform().getWorldMatrix());
        TerrainNode node = (TerrainNode) object;
        setUniformf("gap", node.getGap());
        setUniform("location", node.getLocation());
        setUniformi("lod", node.getLod());
        setUniformf("scaleY", node.getConfig().getScaleY());
        setUniform("index", node.getIndex());
        setUniform("cameraPos", Camera.getInstance().getPosition());
        int loc = GL20.glGetUniformLocation(getProgram(), "lod_morphing_area");
        GL20.glUniform1iv(loc, node.getConfig().getLodMorphing());
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        node.getConfig().getHeightMap().bind();
        setUniformi("heightMap", 0);

        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        node.getConfig().getNormalMap().bind();
        setUniformi("tFactor", node.getConfig().getTessFactor());
        setUniformf("tSlope", node.getConfig().getTessSlope());
        setUniformf("tShift", node.getConfig().getTessShift());

        setUniformi("tbnRange", node.getConfig().getTbnRange());

        int texUnit = 2;
        for (int i=0; i<2; i++){
            GL13.glActiveTexture(GL13.GL_TEXTURE0 + texUnit);
            node.getConfig().getMaterials().get(i).getDisplacemap().bind();
            setUniformi("textures[" + i + "].dispmap", texUnit);
            texUnit++;

            setUniformf("textures[" + i + "].heightScale",
                    node.getConfig().getMaterials().get(i).getDisplaceScale());
            setUniformf("textures[" + i + "].horizontalScale",
                    node.getConfig().getMaterials().get(i).getHorizontalScale());

        }
    }
}
